import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service'

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  lat: number;
  lng: number;
  lats :number;
  lngss: number;
  user: any = {
  };

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.user = this.dataService.getOption();
    this.lats = this.user.lat;
    this.lngss = this.user.lng;

  }
}
