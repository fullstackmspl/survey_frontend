import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service'
import { NotificationService } from '../../services/notification.service'
@Component({
  selector: 'app-gastype',
  templateUrl: './gastype.component.html',
  styleUrls: ['./gastype.component.scss']
})
export class GastypeComponent implements OnInit {
  gasTypeForm: FormGroup;
  submitted = false;
  userResponse: any;
  isLoading = false;
  usersData: any = []
  usersList: any = []
  allUsers: any = []
  deletearray: any = []
  data: any
  search: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getUsers()
    console.log("res", this.getUsers())
    this.gasTypeForm = this.formBuilder.group({
      gasType: ['', Validators.required],
    });
  }
  get f() { return this.gasTypeForm.controls; }

  getUsers() {
    this.isLoading = true;
    this.userService.getGasTypes().subscribe((res) => {
      this.usersList = res
      this.allUsers = this.usersList.data
      this.allUsers.reverse();

      this.isLoading = false;
    });
  }
  onSubmit() {
    this.submitted = true;
    if (this.gasTypeForm.invalid) {
      return;
    }
    this.userService.addGas(this.gasTypeForm.value).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess('GAS ADDED SUCCESFULLY', "")
        this.getUsers()
      } else {
        if (this.userResponse.isSuccess === false) {
          this.notificationService.showError('Offer Not generated', "")
        }
        else {
          // let msg = "Something Went Wrong";
          this.notificationService.showError('SERVER ERROR', "");
        }

      }
    });
  }

  // delete user

  deleteGasType(data) {
    this.userService.deleteGasTypes({ id: data._id }).subscribe(res => {
      this.userResponse = res;
      console.log("response", this.userResponse._id)
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess("GasType Deleted Succesfully", "");
        // this.ngxLoader.stop();
        this.getUsers()
      } else {
        this.notificationService.showError("User not Deleted ", "");
      }
    })
  }

}
