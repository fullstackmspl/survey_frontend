import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GastypeComponent } from './gastype.component';

describe('GastypeComponent', () => {
  let component: GastypeComponent;
  let fixture: ComponentFixture<GastypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GastypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GastypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
