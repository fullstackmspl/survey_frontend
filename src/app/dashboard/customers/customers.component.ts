import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../services/user.service'
import { NotificationService } from '../../services/notification.service'
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataService } from '../../services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CsvDataService } from '../../services/csv-data.service';
// import{ }from '../../../assets/custom.js'
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  dateForm: FormGroup;
  submitted = false;
  isLoading = false;
  usersData: any = []
  newarry: any = []
  usersList: any = []
  allcustomers: any = []
  allcustomerss: any = []
  deletearray: any = []
  dateArray: any = []
  bigCities = [];

  userResponse: any
  data: any
  search: string;
  hello: string;
  startDate: Date;
  endDate: Date;
  fromDate:string;
  toDate:string;
  selectedMembers: string;
  finalDate: string;
  selectedUsers: string;
  //  start = "01-02-2017";
  //  end = "06-07-2017";
  constructor(private userService: UserService, private fb: FormBuilder, private csvService: CsvDataService, private dataService: DataService, private ngxLoader: NgxUiLoaderService, private router: Router, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getCustomers()
    console.log("res", this.getCustomers())
    this.dateForm = this.fb.group({
      fromDate: '',
      toDate: '',
    });
  }

  // get all customers 

  getCustomers() {
    this.isLoading = true;
    this.userService.getCustomer().subscribe((res) => {
      this.usersList = res
      this.allcustomers = this.usersList.data
      this.allcustomers.reverse();
      console.log("list of custometrs",this.allcustomers)
      this.isLoading = false;
      // console.log("res", this.allcustomers)

    });
  }

  // track the customer location

  track(data) {
    this.dataService.setOption(data);
    this.router.navigate(['dashboard/track_customer']);
  }

  // download file

  download() {
    this.csvService.downloadFile(this.allcustomers, 'Total Customers');
  }

refresh(){
  this.getCustomers()
}
onSubmit() {

  this.submitted = true;
  if (this.dateForm.invalid) {
    return;
  }
  
  this.userService.getbyDate(this.dateForm.value).subscribe(res => {
    this.userResponse = res;
    console.log("FINAL RESULT IS ",this.userResponse)
    this.allcustomers = this.userResponse.data
    if (this.userResponse.isSuccess === true) {
      this.notificationService.showSuccess(' Customers Found', "")
    } else {
      if (this.userResponse.isSuccess === false) {
        this.notificationService.showError('No Customers Found', "")
      }
      else {
        this.notificationService.showError('SERVER ERROR', "");
      }

    }
  });
}

}