import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service'

@Component({
  selector: 'app-customer-track',
  templateUrl: './customer-track.component.html',
  styleUrls: ['./customer-track.component.scss']
})
export class CustomerTrackComponent implements OnInit {
  lat: number;
  lng: number;
  lats :number;
  lngs: number;
  user: any = {
  };
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.user = this.dataService.getOption();
    this.lats = this.user.lat;
    this.lngs = this.user.lng;
  }

}
