import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserService } from '../../services/user.service'
import { NotificationService } from '../../services/notification.service'
import { MustMatch } from '../../_shared/mustmatch.validator';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {

  customerForm: FormGroup;
  submitted = false;
  userResponse: any;
  fieldTextType: boolean;
  fieldTextTypes: boolean;

  constructor(private formBuilder: FormBuilder, private router: Router, private notificationService: NotificationService, private userService: UserService) { }

  ngOnInit(): void {
    this.customerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      role: 'U',
    },
      {
        validator: MustMatch('password', 'confirmPassword')
      });


  }

  get f() { return this.customerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.customerForm.invalid) {
      return;
    }
    this.userService.addUsers(this.customerForm.value).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess('User Created Succesfully', "")
        this.router.navigateByUrl('/dashboard/customerDetail');
      } else {
        if (this.userResponse.isSuccess === false) {
          this.notificationService.showError('Email Already Exist', "")
        }
        else {
          // let msg = "Something Went Wrong";
          this.notificationService.showError('SERVER ERROR', "");
        }

      }
    });
  }

  // clearForm() {
  //   this.router.navigateByUrl('/dashboard/customerDetail');

  //   // this.submitted = false;
  //   // this.customerForm.reset();
  // }
  // function for hide show password
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleFieldTextTypes() {
    this.fieldTextTypes = !this.fieldTextTypes;
  }
}

