import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserService } from '../../services/user.service'
import { DataService } from '../../services/data.service'
import { NotificationService } from '../../services/notification.service'
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  editProfileForm: FormGroup;
  submitted = false;
  changePasswordForm: FormGroup;
  userResponse: any;
  fieldTextType: boolean;
  fieldTextTypes: boolean;
  user: any = {
    _id: '',
    name: '',
    email: '',
    password: '',
    phoneNumber: '',
    sex: '',
    address: '',
    role: '',
    city: '',
    country: '',
    zipCode: '',
  };
  constructor(private formBuilder: FormBuilder, private ngxLoader: NgxUiLoaderService, private dataService: DataService, private router: Router, private notificationService: NotificationService, private userService: UserService) { }

  ngOnInit(): void {
    this.user = this.dataService.getOption();

    // update user detail

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
    });

    // change user password

    this.editProfileForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phoneNumber: new FormControl('', Validators.required),
    })


  }
  get f() { return this.editProfileForm.controls; }

  get c() { return this.changePasswordForm.controls; }

  onSubmit() {
    debugger;
    this.submitted = true;
    if (this.editProfileForm.invalid) {
      return;
    }
    this.userService.updateUser(this.user).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess('User Updated Successfully!', '')
        this.router.navigate(['dashboard/customerDetail']);
      } else {
        let msg = "Something Went Wrong";
        this.notificationService.showError('An ERROR OCCURED', '')
      }
    });

  }

  changePassword() {
    this.submitted = true;
    if (this.changePasswordForm.invalid) {
      return;
    }
    let model = {
      _id: this.user,
      oldPassword: this.changePasswordForm.value.oldPassword,
      newPassword: this.changePasswordForm.value.newPassword,
    }
    this.userService.changePassword(model).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === false) {
        this.notificationService.showError('old password do not match', '');
      }
    });
  }

    toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleFieldTextTypes() {
    this.fieldTextTypes = !this.fieldTextTypes;
  }

}