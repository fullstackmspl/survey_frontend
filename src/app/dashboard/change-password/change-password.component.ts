import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service'
import { NotificationService } from '../../services/notification.service'


@Component({
  selector: 'changePassword',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  submitted = false;
  isLoading: boolean;
  user: any;
  userResponse: any;
  userPass: any = {}
  userId: any;
  fieldTextTypes: boolean;
  fieldTextType: boolean;


  constructor(private formBuilder: FormBuilder, private userService: UserService,
    private notificatioService: NotificationService) { }

  ngOnInit(): void {
    this.userId = localStorage._id
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
    });
  }
  get f() { return this.changePasswordForm.controls; }


  onSubmit() {
    this.submitted = true;
    if (this.changePasswordForm.invalid) {
      return;
    }
    let model = {
      _id: this.userId,
      oldPassword: this.changePasswordForm.value.oldPassword,
      newPassword: this.changePasswordForm.value.newPassword,
    }
    this.userService.changePassword(model).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === false) {
        this.notificatioService.showError('old password do not match', '');
      }
    });
  }
  // function for hide show password
  toggleFieldTextTypee() {
    this.fieldTextTypes = !this.fieldTextTypes;
  }

  toggleFieldTextTypeee() {
    this.fieldTextType = !this.fieldTextType;
  }
}
