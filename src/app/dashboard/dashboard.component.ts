import { Component, OnInit } from '@angular/core';
import { RouterOutlet, Router } from '@angular/router';
import { slideInAnimation } from '../animations';
import { NotificationService } from '../services/notification.service';
import { from } from 'rxjs';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    slideInAnimation
    // animation triggers go here
  ]
})
export class DashboardComponent implements OnInit {
  
  constructor(private route: Router, private notificationService:NotificationService) { }

  ngOnInit(): void {
    if (screen.width < 768) {
      let classList = document.getElementById('sidebar').classList;
      classList.toggle('compress');
    }
  }
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
  logout() {
    localStorage.removeItem('apiToken');
    this.notificationService.showInfo("Logout Succesfully","")
      this.route.navigateByUrl('/login')
  }

}
