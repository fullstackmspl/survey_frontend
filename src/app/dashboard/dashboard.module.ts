import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { OffersComponent } from './offers/offers.component';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component'
import { MatTabsModule } from '@angular/material/tabs';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MatButtonModule } from '@angular/material/button';
import { EditUserComponent } from './edit-user/edit-user.component';
import { LocationComponent } from './location/location.component';
import { CustomersComponent } from './customers/customers.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AgmCoreModule } from '@agm/core';
import { GoogleMapsModule } from '@angular/google-maps';
import { CustomerTrackComponent } from './customer-track/customer-track.component';
import { GastypeComponent } from './gastype/gastype.component'

@NgModule({
  declarations: [
    DashboardComponent,
    CustomerDetailComponent,
    AddCustomerComponent,
    OffersComponent,
    ProfileComponent,
    ChangePasswordComponent,
    EditUserComponent,
    LocationComponent,
    CustomersComponent,
    CustomerTrackComponent,
    GastypeComponent,
      ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    FormsModule, ReactiveFormsModule, MatTabsModule, MatButtonModule,
    Ng2SearchPipeModule,
   GoogleMapsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCgd-rD47NFwKVpQ30skw_D-qWUMHrxjO4',
    })
  ]
})
export class DashboardModule { }
