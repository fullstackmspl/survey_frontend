import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { OffersComponent } from './offers/offers.component';
import { ProfileComponent } from './profile/profile.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { LocationComponent } from './location/location.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerTrackComponent } from './customer-track/customer-track.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { GastypeComponent } from './gastype/gastype.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children: [
      { path: 'customerDetail', component: CustomerDetailComponent, data: { animation: 'HomePage' } },
      { path: 'addCustomer', component: AddCustomerComponent, data: { animation: 'AboutPage' } },
      { path: 'offers', component: OffersComponent, data: { animation: 'AboutPage' } },
      { path: 'profile', component: ProfileComponent},
      { path: 'edit_User', component: EditUserComponent},
      { path: 'location', component: LocationComponent},
      { path: 'customers', component: CustomersComponent},
      { path: 'track_customer', component: CustomerTrackComponent},
      { path: 'change_password', component: ChangePasswordComponent},
      { path: 'gas_type', component: GastypeComponent},
      
      
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
