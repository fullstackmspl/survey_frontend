import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service'
import { NotificationService } from '../../services/notification.service'
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataService } from '../../services/data.service'


@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss'],
})
export class CustomerDetailComponent implements OnInit {
  isLoading = false;
  usersData: any = []
  usersList: any = []
  allUsers: any = []
  deletearray: any = []
  userResponse: any
  data: any
  // searchText:'';
  search: string;

  constructor(private userService: UserService, private dataService: DataService, private ngxLoader: NgxUiLoaderService, private router: Router, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getUsers()
    console.log("res", this.getUsers())
  }

  // get users

  getUsers() {
    this.isLoading = true;
    this.userService.getUsers().subscribe((res) => {
      this.usersList = res
      this.allUsers = this.usersList.data
      this.allUsers.reverse();

      this.isLoading = false;
    });
  }

  // delete user

  deleteUser(data) {
    this.userService.deleteUser({ id: data._id }).subscribe(res => {
      this.userResponse = res;
      console.log("response", this.userResponse._id)
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess("User Deleted Succesfully", "");
        this.ngxLoader.stop();
        this.getUsers()
      } else {
        this.notificationService.showError("User not Deleted ", "");
      }
    })
  }

  // edit User
  edit(data) {
    debugger;
    this.dataService.setOption(data);
    this.router.navigate(['dashboard/edit_User']);
  }

  loc(data) {
    this.dataService.setOption(data);
    this.router.navigate(['dashboard/location']);
  }

}