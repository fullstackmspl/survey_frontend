import { Component, OnInit } from '@angular/core';
// import { invalid } from '@angular/compiler/src/render3/view/util';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service'
import { NotificationService } from '../../services/notification.service'
@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  offerForm: FormGroup;
  submitted = false;
  userResponse: any;
  isLoading = false;
  usersData: any = []
  usersList: any = []
  allUsers: any = []
  deletearray: any = []
  data: any
  search: string;
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService, private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.getOffers()

    this.offerForm = this.formBuilder.group({
      offers: ['', Validators.required],
    });

  }
  get f() { return this.offerForm.controls; }

  getOffers() {
    this.isLoading = true;
    this.userService.getoffers().subscribe((res) => {
      this.usersList = res
      this.allUsers = this.usersList.data
      this.allUsers.reverse();
      this.isLoading = false;
    });
  }

  // delete user

  deleteGasType(data) {
    this.userService.deleteOffers({ id: data._id }).subscribe(res => {
      this.userResponse = res;
      console.log("response", this.userResponse._id)
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess("Offer Deleted Succesfully", "");
        this.getOffers()
      } else {
        this.notificationService.showError("User not Deleted ", "");
      }
    })
  }

  onSubmit() {
    this.submitted = true;
    if (this.offerForm.invalid) {
      return;
    }
    this.userService.addOffers(this.offerForm.value).subscribe(res => {
      this.userResponse = res;
      if (this.userResponse.isSuccess === true) {
        this.notificationService.showSuccess('OFFER ADDED SUCCESFULLY', "")
        this.getOffers()
      } else {
        if (this.userResponse.isSuccess === false) {
          this.notificationService.showError('Offer Not generated', "")
        }
        else {
          // let msg = "Something Went Wrong";
          this.notificationService.showError('SERVER ERROR', "");
        }

      }
    });
  }
}

