import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service'
import { NotificationService } from '../services/notification.service'
import { NgxUiLoaderService } from 'ngx-ui-loader';

// import{subscribe} from
import { from } from 'rxjs';
import { invalid } from '@angular/compiler/src/render3/view/util';
@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.scss']
})
export class AdminloginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  isLoading: boolean;
  userData: any = [];
  fieldTextType: boolean;
  credentials = {
    email: '',
    password: ''

  };
  constructor(private formBuilder: FormBuilder, private router: Router, private ngxLoader: NgxUiLoaderService,
    private userService: UserService, private notificatioService: NotificationService) { }

  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }



  get f() { return this.loginForm.controls; }

  // login form submit function

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.ngxLoader.start();
    this.userService.login(this.loginForm.value)
      .subscribe(res => {
        if (res.isSuccess === false) {
          this.ngxLoader.stop();
          this.notificatioService.showError("Invalid Username or password", "")
          return this.isLoading = false;
        } else {
          this.userData = res;
          this.isLoading = false;
          if (this.userData.role === "A") {
            this.ngxLoader.stop();
            localStorage.setItem('apiToken', this.userData.apiToken);
            localStorage.setItem('_id', this.userData._id);
            this.notificatioService.showSuccess("Login Succesfully", "")
            this.router.navigateByUrl('/dashboard/customerDetail');
          }
          else {
            this.notificatioService.showError("Invalid Username or password", "")
            return this.isLoading = false;
          }
        }
      },
      )
  }

  // for password view hide

  toggleFieldTextTypess() {
    this.fieldTextType = !this.fieldTextType;
  }
}

