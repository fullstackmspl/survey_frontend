import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SplashComponent } from './splash/splash.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
  import { from } from 'rxjs';
  import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes: Routes = [
  //module path
  // component routes
  {path: '', component: SplashComponent},
  { path: 'login', component: AdminloginComponent
},
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
