export interface userPassword {
    _id: string;
    newPassword: string;
    oldPassword: string;
}
