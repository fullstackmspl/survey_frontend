export interface User {
    _id: string;
    isSuccess: boolean;
    apiToken: String;
    email: string;
    password: string;
}
