import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError, Subject, from, pipe } from 'rxjs';
import { User } from '../models/user';
import { userPassword } from '../models/userPassword';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { NotificationService } from '../services/notification.service'



@Injectable({
  providedIn: 'root'
})
export class UserService {
  confirm(arg0: { message: string; }) {
    throw new Error("Method not implemented.");
  }
  root = environment.apiUrl;
  userResponse: any = {};
  responseData: any;
  responseDatas: any;

  token: string = ''
  constructor(private http: HttpClient, private route: Router, private notificatioService: NotificationService) { }

  getHeaders() {
    let header
    if (this.token != '') {
      header = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'x-access-token': this.token
        })
      }
    } else {
      console.log('token not found')
    }
    return header;

  }
  //login 

  login(model): Observable<User> {
    const subject = new Subject<User>();
    this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {
      subject.next(responseData.data);
    }, (error) => {
      subject.next(error.error);
    });

    return subject.asObservable();

  }

  // register

  addUsers(data): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/create`, data).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  // change Password

  changePassword(model): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/changePassword`, model).subscribe((res) => {
      this.userResponse = res;
      this.notificatioService.showSuccess('Password Updated Successfully!', '');
      this.route.navigateByUrl('/dashboard/customerDetail');
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  // get allUsers

  getUsers(): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.get(`${this.root}/users/getUsers`).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    });
    return subject.asObservable();
  }

  // get allUsers

  getGasTypes(): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.get(`${this.root}/users/getGasType`).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    });
    return subject.asObservable();
  }

  // get allUsers

  getoffers(): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.get(`${this.root}/users/getOffers`).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    });
    return subject.asObservable();
  }


  deleteOffers(id): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/deleteOffers`, id, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);

    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  getbyDate(data) {
    const subject = new Subject<any[]>();
    this.http.get(`${this.root}/users/getProvidersByDate?fromDate=${data.fromDate}&toDate=${data.toDate}`).subscribe((response: any) => {
      this.userResponse = response;
      console.log("service response",response)
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }


  getCustomer(): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.get(`${this.root}/users/getDetails`).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    });
    return subject.asObservable();
  }


  // delete user

  deleteUser(id): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/delete`, id, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);

    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  // delete user

  deleteGasTypes(id): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/deleteGasType`, id, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);

    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }


  addOffers(data): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/addOffers`, data).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  // update user
  updateUser(model): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/update`, model, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  updateCustomer(model): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/updateCustomer`, model, { headers: null }).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }


  addGas(data): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/addGasType`, data).subscribe((responseData) => {
      this.userResponse = responseData;
      subject.next(this.userResponse);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }
}

// add Customers
