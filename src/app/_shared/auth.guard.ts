import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from 
'@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../services/user.service';
import {Router} from '@angular/router';
import { logging } from 'protractor';
import { invalid } from '@angular/compiler/src/render3/view/util';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private users: UserService,
    private myRoute: Router){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.users.login(){
      return true;
    }else{
      this.myRoute.navigate(["login"]);
      return false;
    }
  }
}